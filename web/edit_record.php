<?php


require_once '../db/database.php';
$settings = include_once '../db/db_settings.php';
$database = new Database($settings['servername'], $settings['username'], $settings['password'], $settings['dbname'], $settings['port']);
$conn = $database->connect();
$test_id = $_GET['test_id'];


$sql = "SELECT * FROM tests t
INNER JOIN test_details td on t.id = td.test_id
LEFT JOIN test_steps ts on t.id = ts.test_id
WHERE t.id = $test_id";


$res_data = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res_data)) {
    $dbData = $row;
    $steps[] = $dbData['steps'];

};

if (isset($dbData)) {
    $page_title = $dbData['summary'];


    include_once 'layout_header.php';

    echo "

<ul class='list-group'>
    <li class='list-group-item'><p>Status: " . $dbData['status'] . "</p></li>

</ul>
<form action='' method='post'>
<table class='table table-hover table-responsive table-bordered'><tr><td><div align='left'><a href='edit_record.php?test_id=" . $test_id . "' class='btn btn-primary'>Edit</a></div></td></tr><tr></table>
<fieldset disabled>

    <table class='table table-hover table-responsive table-bordered'>
    
            <td>Reporter</td>
            <td><input type='text' name='reporter' class='form-control' value='" . $dbData['reporter'] . "'/></td>
        </tr>
        <tr>
            <td>Summary</td>
            <td><input type='text' name='summary' class='form-control' value='" . $dbData['summary'] . "'/></td>
        </tr>
        <tr>
            <td>Description</td>
            <td><textarea name='description' class='form-control'>" . $dbData['description'] . " </textarea></td>
        </tr>
        <td>Steps</td>
        ";
    $i = 0;
    foreach ($steps as $detail) {
        $i++;

        echo $step = "<tr>
            <td align='left'>" . $i . ".</td>
            <td><input type='text' name='steps' class='form-control' value='$detail'/></td>
            
        </tr>";
    }
    $next_id = $test_id + 1;
    $back_id = $test_id - 1;

    echo "</table>
 
</fieldset>
<div align='left'><a href='read_record.php?test_id=" . $back_id . "' class='btn btn-primary'>Back</a></div> 
<div align='right'><a href='read_record.php?test_id=" . $next_id . "' class='btn btn-primary'>Next</a></div> 
</form>";
} else {

    echo "<h1>Sum Ting Wong!</h1>";
}

include_once 'layout_menu.php';
include_once 'layout_footer.php'; ?>
