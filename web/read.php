<?php
/**
 * Created by PhpStorm.
 * User: armando.gavrila
 * Date: 10/5/2018
 * Time: 1:35 PM
 */
require_once '../db/database.php';
$settings = include_once '../db/db_settings.php';

if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}
$no_of_records_per_page = 2;
$offset = ($pageno - 1) * $no_of_records_per_page;

$database = new Database($settings['servername'], $settings['username'], $settings['password'], $settings['dbname'], $settings['port']);
$conn = $database->connect();


$total_pages_sql = "SELECT COUNT(*) FROM tests";
$result = mysqli_query($conn, $total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
$total_pages = ceil($total_rows / $no_of_records_per_page);

$sql = "SELECT t.id,reporter, status, summary FROM tests t 
INNER JOIN test_details td ON t.id = td.test_id
WHERE t.reporter = 'armando.gavrila' AND t.status!='Deleted' LIMIT $offset, $no_of_records_per_page";
$res_data = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res_data)) {

    echo " 
 
 
 <div class='table'> <table class='table table-hover table-responsive table-bordered'>
 
  

  
 <tr>
            
                <td>Reporter</td>
                <td width='50%'><p>" . $row['reporter'] . "</p></td>
            
                <td width='10%'<p align='left'>
                
                
                
                
                <form action='edit_record.php'>
                <input type='hidden' value='" . $row['id'] . "' name = 'test_id'>
                
                <button class='btn btn-info btn-md 'type='submit' value='update'style='width: 100%'><span class='glyphicon glyphicon-floppy-disk'></span> Edit</button>
                
                
                
                </form>
                
                
                
                
                
                
                </p></td>
                
               
            </tr>
           

            <tr>
            
                <td>Summary</td>
                <td width=\"50%\"><p>" . substr($row['summary'], 0, 100) . "..." . "</p></td>
                <td width='10%'<p align='left'>
                
                
                
                
                <form action='read_record.php'>
                <input type='hidden' value='" . $row['id'] . "' name = 'test_id'>
                
                <button class='btn btn-info btn-md 'type='submit' value='update'style='width: 100%'><span class='glyphicon glyphicon-book'></span> Read</button>
                
                
                
                </form>
                
                
                
                
                
                
                </p></td>
            </tr>
            <tr>
                <td>Status</td>
                <td width='50%'><p>" . ($row['status']) . "</p></td>
                
        <td width='10%'><p align='left'><button class='btn btn-info btn-md 'type='submit' value='update'style='width: 100%'>
          <span class='glyphicon glyphicon-trash'></span> Delete
        </button></p></td>
            </tr>   

            <tr>

            </tr>

             
            
        </table></div>";;
}

mysqli_close($conn);
?>
<ul class="pagination">
    <li><a href="?pageno=1">First</a></li>
    <li class="<?php if ($pageno <= 1) {
        echo 'disabled';
    } ?>">
        <a href="<?php if ($pageno <= 1) {
            echo '#';
        } else {
            echo "?pageno=" . ($pageno - 1);
        } ?>">Prev</a>
    </li>
    <li class="<?php if ($pageno >= $total_pages) {
        echo 'disabled';
    } ?>">
        <a href="<?php if ($pageno >= $total_pages) {
            echo '#';
        } else {
            echo "?pageno=" . ($pageno + 1);
        } ?>">Next</a>
    </li>
    <li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
</ul>
