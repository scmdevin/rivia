<?php

$page_title = 'Add test steps';
include_once "layout_header.php";
include_once "../db/insert.php";


?>
    <script type="text/javascript">
        $(document).ready(function () {
            var maxField = 10;
            var addButton = $('.add_button');
            var wrapper = $('.field_wrapper');
            var fieldHTML = "<div><tr><td><textarea type='textarea' name='steps[]' class='form-control table table-hover table-responsive table-bordered new' required></textarea><a href=\"javascript:void(0);\" class=\"btn btn-primary remove_button\"><span\n" +
                "                                class=\"glyphicon glyphicon-minus\"></span> Remove</a></div></td></tr>";


            var x = 1;


            $(addButton).click(function () {

                if (x = 1) {
                    $(wrapper).append("<br>");
                }

                if (x < maxField) {
                    x++;

                    $(wrapper).append(fieldHTML);


                }
            });


            $(wrapper).on('click', '.remove_button', function (e) {
                e.preventDefault();
                $(this).parent('div').fadeOut(function () {
                    $(this).remove()
                });
                x--;
            });
        });
    </script>

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

        <table class='table table-hover table-responsive table-bordered'>


            <tr>

                <td>Steps</td>
                <td>
                    <div class="field_wrapper"><textarea type='textarea' name='steps[]' class='form-control'
                                                         required></textarea>

                </td>
                <td>
                    <button class="btn btn-primary add_button" type=button><span
                                class="glyphicon glyphicon-plus"></span> Add
                    </button>
                </td>


            </tr>


            <tr>

                <td>
                    <button type="submit" class="btn btn-primary">Finalize</button>

                </td>
            </tr>
            <input type=hidden name="user" value=<?php echo "armando.gavrila" ?>>
        </table>


    </form>


<?php
$settings = include '../db/db_settings.php';
$database = new Database($settings['servername'], $settings['username'], $settings['password'], $settings['dbname'], $settings['port']);
$conn = $database->connect();

$insertStep = new InsertInDatabase($conn);

if (session_start()) {
    $last_id = $_SESSION['last_id'];
    $insertStep->insertSteps($last_id);
    $database->disconnect($conn);
} else {
    echo "Go back to last page";
}
include_once "layout_footer.php"; ?>