<?php
/**
 * Created by PhpStorm.
 * User: armando.gavrila
 * Date: 19-Jul-18
 * Time: 16:29
 */

$page_title = 'Create Test';

include_once 'layout_header.php';
include_once '../db/insert.php';

?>


    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

        <table class='table table-hover table-responsive table-bordered'>


            <tr>
                <td>Summary</td>
                <td><input type='text' name='summary' class='form-control' required/></td>
            </tr>

            <tr>
                <td>Description</td>
                <td><textarea name='description' class='form-control' required></textarea></td>
            </tr>

            <tr>

            </tr>

            <tr>

                <td>
                    <button type="submit" class="btn btn-primary">Next</button>

                </td>
            </tr>
            <input type=hidden name="user" value=<?php echo "armando.gavrila" ?>>
        </table>


    </form>


<?php

$settings = include '../db/db_settings.php';
$database = new Database($settings['servername'], $settings['username'], $settings['password'], $settings['dbname'], $settings['port']);
$conn = $database->connect();

$insert = new InsertInDatabase($conn);
$last_id = $insert->insertTest();

$insert->insertTestDetails($last_id);
session_start();
$_SESSION['last_id'] = $last_id;

include_once "layout_footer.php"; ?>