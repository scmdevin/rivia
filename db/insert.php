<?php
/**
 * Created by PhpStorm.
 * User: armando.gavrila
 * Date: 20-Jul-18
 * Time: 16:10
 */

require_once 'database.php';



class InsertInDatabase
{

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function insertTest()
    {



        if ($_POST) {

            $sql = "INSERT INTO tests (`reporter`,`status`) VALUES  ('" . $_POST['user'] . "','open')";


            if (mysqli_query($this->conn, $sql)) {
                echo "<div class='alert alert-success'>Test was created.</div>";
                return $last_id = $this->conn->insert_id;
            } else {
                echo "Error: " . $sql . "" . mysqli_error($this->conn);
            }

        }


    }

    public function insertTestDetails($last_id)
    {
        if ($_POST) {
            $summary = $_POST['summary'];
            $description = $_POST['description'];
            $sql = "INSERT INTO `rivia`.`test_details` (`test_id`, `summary`, `description`) VALUES ('" . $last_id . "', '" . $summary . "', '" . $description . "')";


            if (mysqli_query($this->conn, $sql)) {
                echo "<div class='alert alert-success'>Test was created.</div>";
                header("Location: create_test_steps.php");

            } else {
                echo "Error: " . $sql . "" . mysqli_error($this->conn);
            }

        }


    }

    public function insertSteps($last_id)
    {
        if ($_POST) {

            $step = $_POST['steps'];

            foreach ($step as $stepContent) {

                $sql = "INSERT INTO `rivia`.`test_steps` (`test_id`, `steps`) VALUES ('" . $last_id . "', '" . $stepContent . "');";


                if (mysqli_query($this->conn, $sql)) {
                    echo "<div class='alert alert-success'>Test was created.</div>";

                } else {
                    echo "Error: " . $sql . "" . mysqli_error($this->conn);
                }

            }
        }

    }

}





