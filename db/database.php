<?php
/**
 * Created by PhpStorm.
 * User: armando.gavrila
 * Date: 19-Jul-18
 * Time: 16:33
 */

class Database {
    function __construct($servername,$username,$password,$dbname,$port)
    {
        $this->servername = $servername;
        $this->username = $username;
        $this->password = $password;
        $this->port=$port;
        $this->dbname = $dbname;



    }

    function connect ()
    {
        $conn = new mysqli($this->servername,$this->username,$this->password,$this->dbname,$this->port);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }

    function disconnect($conn)
    {
        mysqli_close($conn);
    }
}
